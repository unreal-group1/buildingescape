// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "C_PlayerController.generated.h"

class UInputMappingContext;

/**
 * 
 */
UCLASS()
class BUILDINGESCAPE_API AC_PlayerController : public APlayerController
{
	GENERATED_BODY()
};
