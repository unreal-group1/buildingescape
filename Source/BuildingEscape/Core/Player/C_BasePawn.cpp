// Fill out your copyright notice in the Description page of Project Settings.

#include "C_BasePawn.h"

#include "GameFramework/FloatingPawnMovement.h"
#include "InputMappingContext.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "Components/CapsuleComponent.h"

AC_BasePawn::AC_BasePawn()
{
 	PrimaryActorTick.bCanEverTick = true;

	FloatingPawnMovement
		= CreateDefaultSubobject<UFloatingPawnMovement>("FloatingPawnMovement");

	CapsuleComponent
		= CreateDefaultSubobject<UCapsuleComponent>("CapsuleComponent");
	if (CapsuleComponent)
	{
		CapsuleComponent->InitCapsuleSize(35.f, 96.0f);
		if (GetOwner()
			&& GetOwner()->GetRootComponent())
		{
			CapsuleComponent->SetupAttachment(GetOwner()->GetRootComponent());
		}
		CapsuleComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"), false);
	}
}

void AC_BasePawn::BeginPlay()
{
	Super::BeginPlay();

	APlayerController* PlayerController = Cast<APlayerController>(GetController());

	if (PlayerController)
	{
		UEnhancedInputLocalPlayerSubsystem* Subsystem
			= ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());

		if (Subsystem)
		{
			Subsystem->ClearAllMappings();
			if (InputMapping)
			{
				Subsystem->AddMappingContext(InputMapping, 0);
			}
		}
	}
}

void AC_BasePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AC_BasePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* Input 
		= Cast<UEnhancedInputComponent>(PlayerInputComponent);

	if (Input)
	{
		if (InteractAction)
		{
			Input->BindAction(
				InteractAction,
				ETriggerEvent::Started,
				this,
				&AC_BasePawn::Interact);
		}
		if (MoveAction)
		{
			Input->BindAction(
				MoveAction,
				ETriggerEvent::Triggered,
				this,
				&AC_BasePawn::Move);
		}
		if (LookAction)
		{
			Input->BindAction(
				LookAction,
				ETriggerEvent::Triggered,
				this,
				&AC_BasePawn::Look);
		}
	}
}

void AC_BasePawn::Interact()
{
	bIsInteract = !bIsInteract;
	OnInteract.Broadcast(bIsInteract);
}

void AC_BasePawn::Move(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller
		&& FloatingPawnMovement)
	{
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void AC_BasePawn::Look(const FInputActionValue& Value)
{
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller)
	{
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(-LookAxisVector.Y);
	}
}

