// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "C_BasePawn.generated.h"

class UInputAction;
class UInputMappingContext;
class UFloatingPawnMovement;
class UCapsuleComponent;
struct FInputActionValue;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteract, bool, bIsInreract);

UCLASS()
class BUILDINGESCAPE_API AC_BasePawn : public APawn
{
	GENERATED_BODY()

public:
	AC_BasePawn();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "EnhancedInput")
	UInputMappingContext* InputMapping = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UCapsuleComponent* CapsuleComponent = nullptr;
//Interact
public:
	UPROPERTY(BlueprintAssignable)
	FOnInteract OnInteract;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UInputAction* InteractAction = nullptr;

private:
	bool bIsInteract = false;

	void Interact();
//Noving
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UInputAction* MoveAction = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UInputAction* LookAction = nullptr;
private:
	void Move(const FInputActionValue& Value);
	void Look(const FInputActionValue& Value);
	UPROPERTY(VisibleAnywhere)
	UFloatingPawnMovement* FloatingPawnMovement = nullptr;

};
