// SMOG

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

#include "C_GrabberComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UC_GrabberComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UC_GrabberComponent();

protected:
	virtual void BeginPlay() override;
	//������������� ����� ������ (���, ����������� � �.�.)
	void InitGlobalData();

public:	
	virtual void TickComponent(
		float DeltaTime, 
		ELevelTick TickType, 
		FActorComponentTickFunction* ThisTickFunction) override;

private:
	UWorld* World;
	APlayerController* PlayerController;
	//�������� ��� � ������� ���������� �������
	UFUNCTION()
	void Grab(bool bIsGrab);
	//Line
protected:
	//��������� ����� � ���� �������� � ������ ������ ������
	void GetViewPoint(
		FVector& NewPlayerViewPointLocation,
		FRotator& NewPlayerViewPointRotation);
	//��������� ����� ����, ��� ��������
	FVector GetLineStartPoint();
	//�������� ����� ����, ��� ��������
	FVector GetLineEndPoint();
	//DrawViewLine
protected:
	UPROPERTY(EditAnywhere)
	float LineLength = 150.0f;
	//������������ ����� �������� ��� �������
	void DrawViewLine(
		FVector LineTraceStart, 
		FVector LineTraceEnd);
	//��������� ������� ����������� ������� ��� ��������
	const FHitResult GetFirstPhysicsBodyInLangth();

	//�������������� � ���������� ������
private:
	UPhysicsHandleComponent* PhysicsHandleComponent = nullptr;
	UInputComponent* InputComponent = nullptr;
	//�������� ������� ������ �������� 
	void SetupInputGrabComponent();
};
