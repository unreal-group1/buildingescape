#include "C_PositionReporterComponent.h"

UC_PositionReporterComponent::UC_PositionReporterComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UC_PositionReporterComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UC_PositionReporterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

