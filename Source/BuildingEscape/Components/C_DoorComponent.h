#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"

#include "C_DoorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UC_DoorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UC_DoorComponent();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(BlueprintAssignable)
	FOnDoorEvent OnOpen;
	UPROPERTY(BlueprintAssignable)
	FOnDoorEvent OnClose;

	virtual void TickComponent(
		float DeltaTime, 
		ELevelTick TickType, 
		FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere)
	float OpenAngle = 0.0f;
	UPROPERTY(EditAnywhere)
	float CloseAngle = 90.0f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate = nullptr;

	AActor* ActorOwner = nullptr;
	//Mass for open door
public:
	UPROPERTY(EditAnywhere)
	float ActorsOnPlateMassForOpenDoor = 30.0f;
	//Returns mass on kg
	float GetTotalMassOfActorsOnPlate();
};
