// SMOG

#include "C_DoorComponent.h"

#define OUT

UC_DoorComponent::UC_DoorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UC_DoorComponent::BeginPlay()
{
	Super::BeginPlay();

	ActorOwner = GetOwner();
}

void UC_DoorComponent::TickComponent(
	float DeltaTime, 
	ELevelTick TickType, 
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(
		DeltaTime, 
		TickType, 
		ThisTickFunction);

	if (GetTotalMassOfActorsOnPlate() > ActorsOnPlateMassForOpenDoor) 
	{
		OnOpen.Broadcast();
	}
	else
	{
		OnClose.Broadcast();
	}
}


float UC_DoorComponent::GetTotalMassOfActorsOnPlate()
{
	float TotalMass = 0.0f;
	TArray<AActor*> ActorsOnPlate;
	if (PressurePlate)
	{
		PressurePlate->GetOverlappingActors(OUT ActorsOnPlate);

		if (ActorsOnPlate.Num() > 0)
		{
			for (const auto& ActorOnPlate : ActorsOnPlate)
			{
				auto PrimitiveComponent = ActorOnPlate->FindComponentByClass<UPrimitiveComponent>();
				if (PrimitiveComponent)
				{
					TotalMass += PrimitiveComponent->GetMass();
				}
			}
		}
	}

	return TotalMass;
}