// SMOG

#include "C_GrabberComponent.h"
#include "C_BasePawn.h"

#define OUT

UC_GrabberComponent::UC_GrabberComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	PhysicsHandleComponent
		= CreateDefaultSubobject<UPhysicsHandleComponent>("PhysicsHandleComponent");
}

void UC_GrabberComponent::BeginPlay()
{
	Super::BeginPlay();

	InitGlobalData();

	check(PhysicsHandleComponent);

	SetupInputGrabComponent();
}

void UC_GrabberComponent::InitGlobalData()
{
	if (!World)
	{
		World = GetWorld();
	}

	if (!PlayerController)
	{
		PlayerController = GetWorld()->GetFirstPlayerController();
	}
}

void UC_GrabberComponent::GetViewPoint(
	FVector& NewPlayerViewPointLocation, 
	FRotator& NewPlayerViewPointRotation)
{
	if (World
		&& PlayerController)
	{
		PlayerController->GetPlayerViewPoint(
			OUT NewPlayerViewPointLocation, //������� OUT ��� ���������, ��� ���������
			OUT NewPlayerViewPointRotation); //���������� ��������
	}
	else
	{
		InitGlobalData();

		PlayerController->GetPlayerViewPoint(
			OUT NewPlayerViewPointLocation, //������� OUT ��� ���������, ��� ���������
			OUT NewPlayerViewPointRotation); //���������� ��������
	}
}

FVector UC_GrabberComponent::GetLineStartPoint()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation);

	return PlayerViewPointLocation;
}

FVector UC_GrabberComponent::GetLineEndPoint()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation);

	return PlayerViewPointLocation
		+ (PlayerViewPointRotation.Vector()
			* LineLength);
}

void UC_GrabberComponent::DrawViewLine(
	FVector LineTraceStart,
	FVector LineTraceEnd)
{
	FColor LineColor = FColor(
		255, 
		0, 
		0, 
		1);

	if (World
		&& PlayerController)
	{
		DrawDebugLine(
			World,
			LineTraceStart,
			LineTraceEnd,
			LineColor,
			false,
			0.0f,
			0,
			2.0f);
	}
	else
	{
		InitGlobalData();
	}
}

void UC_GrabberComponent::TickComponent(
	float DeltaTime, 
	ELevelTick TickType, 
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(
		DeltaTime, 
		TickType, 
		ThisTickFunction);

	if (PhysicsHandleComponent
		&& PhysicsHandleComponent->GrabbedComponent)
	{
		PhysicsHandleComponent->SetTargetLocation(GetLineEndPoint());
	}
}

void UC_GrabberComponent::Grab(bool bIsGrab)
{
	auto HitResult = GetFirstPhysicsBodyInLangth();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();

	if (bIsGrab)
	{
		if (PhysicsHandleComponent
			&& ComponentToGrab
			&& ActorHit)
		{
			PhysicsHandleComponent->GrabComponent(
				ComponentToGrab,
				NAME_None, //No bones needed
				ActorHit->GetActorLocation(),
				true);
		}
	}
	else
	{
		if (PhysicsHandleComponent)
		{
			PhysicsHandleComponent->ReleaseComponent();
		}
	}
}

const FHitResult UC_GrabberComponent::GetFirstPhysicsBodyInLangth()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation);

	FCollisionQueryParams LineTraceParams;
	FCollisionObjectQueryParams ObjectQueryParams;
	FHitResult HitLineTrace;

	ObjectQueryParams = FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody);
	//������������� ������ ����
	LineTraceParams = FCollisionQueryParams(
		FName(TEXT("")),
		false,
		GetOwner());

	if (World)
	{
		World->LineTraceSingleByObjectType(
			HitLineTrace,
			GetLineStartPoint(),
			GetLineEndPoint(),
			ObjectQueryParams,
			LineTraceParams);

		return HitLineTrace;
	}

	return HitLineTrace;
}

void UC_GrabberComponent::SetupInputGrabComponent()
{
	if (GetOwner())
	{
		AC_BasePawn* CurrentPawn = Cast<AC_BasePawn>(GetOwner());
		if (CurrentPawn)
		{
			CurrentPawn->OnInteract.AddDynamic(
				this,
				&UC_GrabberComponent::Grab);
		}
	}
}

